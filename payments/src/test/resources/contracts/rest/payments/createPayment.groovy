package contracts.rest

import org.springframework.cloud.contract.spec.Contract
import org.springframework.cloud.contract.spec.internal.HttpMethods

Contract.make {
	request {
		method(HttpMethods.HttpMethod.POST)
		url("/payments")
	}

	response {
		status(CREATED())
		headers {
			header(location(), "http://payments/payments/123")
		}
	}
}