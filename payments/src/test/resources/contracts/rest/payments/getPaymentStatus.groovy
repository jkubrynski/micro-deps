package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		method(GET())
		url("/payments/abc")
	}

	response {
		status(OK())
		body(
				"OK"
		)
	}
}