package com.ing.legato.auctions;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.StubFinder;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(ids = ":payments")
public class PaymentServiceTest {

	@Autowired
	StubFinder stubFinder;

	@Autowired
	PaymentsQueueListener paymentsQueueListener;

	@Test
	public void shouldReceivePaymentNotification() {
		stubFinder.trigger("paymentReceived");

		Payment lastMessage = paymentsQueueListener.getLastMessage();
		assertThat(lastMessage.getPaymentId()).isEqualTo("abc");
		assertThat(lastMessage.getStatus()).isEqualTo(PaymentStatus.PAID);
	}

}
