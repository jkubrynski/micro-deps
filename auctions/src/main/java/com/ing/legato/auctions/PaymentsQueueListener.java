package com.ing.legato.auctions;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class PaymentsQueueListener {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private Payment lastPayment;

	@StreamListener(PaymentsQueue.QUEUE_NAME)
	void handleMessage(Payment payment) {
		LOG.info("Received message: {}", payment);
		lastPayment = payment;
	}

	public Payment getLastMessage() {
		return lastPayment;
	}
}
