package com.ing.legato.payments;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMessageVerifier
public abstract class BaseContractTest {

	@Autowired
	PaymentService paymentService;

	@Before
	public void setUp() {
		PaymentService paymentService = Mockito.mock(PaymentService.class);

		configureDataSet(paymentService);

		RestAssuredMockMvc.standaloneSetup(new PaymentsController(paymentService));
	}

	private void configureDataSet(PaymentService paymentService) {
		when(paymentService.getPaymentStatus("abc")).thenReturn("OK");
		when(paymentService.createPayment()).thenReturn("123");
	}

	protected void triggerPaymentReceived() {
		paymentService.paymentReceived("abc");
	}
}
