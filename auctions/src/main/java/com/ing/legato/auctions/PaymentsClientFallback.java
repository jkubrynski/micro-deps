package com.ing.legato.auctions;

import org.springframework.stereotype.Component;

@Component
class PaymentsClientFallback implements PaymentsClient {

	@Override
	public String getStatus(String id) {
		return "Unknown";
	}
}
