package com.ing.legato.auctions;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

interface PaymentsQueue {

	String QUEUE_NAME = "paymentsQueue";

	// CHECK!!!!!
	@Input(QUEUE_NAME)
	MessageChannel paymentsQueue();

}
