package com.ing.legato.auctions;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auctions")
class AuctionController {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final AuctionService auctionService;

	AuctionController(AuctionService auctionService) {
		this.auctionService = auctionService;
	}

	@PostMapping
	void buy(@RequestBody PurchaseRequest purchaseRequest) {
		auctionService.buy(purchaseRequest);
	}

	@GetMapping("/{id}")
	String getStatus(@PathVariable  String id) {
		LOG.info("Received payment status request: {}", id);
		return auctionService.getStatus(id);
	}

}
