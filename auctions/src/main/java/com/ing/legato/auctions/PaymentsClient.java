package com.ing.legato.auctions;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "payments", fallback = PaymentsClientFallback.class)
interface PaymentsClient {
	@RequestMapping(method = RequestMethod.GET, value = "/payments/{id}")
	String getStatus(@PathVariable("id") String id);
}
