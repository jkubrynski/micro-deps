package com.ing.legato.payments;

class Payment {

	private final String paymentId;
	private final PaymentStatus status;

	Payment(String paymentId, PaymentStatus status) {
		this.paymentId = paymentId;
		this.status = status;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public PaymentStatus getStatus() {
		return status;
	}
}
