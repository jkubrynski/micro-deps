package com.ing.legato.auctions;

import java.lang.invoke.MethodHandles;
import java.net.URI;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
class AuctionService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentsClient paymentsClient;

	AuctionService(PaymentsClient paymentsClient) {
		this.paymentsClient = paymentsClient;
	}

	String getStatus(String id) {
		LOG.info("Invoking payments client: {}", id);
		paymentsClient.getStatus(id);
		paymentsClient.getStatus(id);
		return paymentsClient.getStatus(id);
	}

	void buy(PurchaseRequest purchaseRequest) {
//		URI uri = restTemplate
//				.postForLocation("http://payments/payments",
//						purchaseRequest);
	}
}
