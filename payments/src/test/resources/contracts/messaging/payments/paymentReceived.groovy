package contracts.messaging.payments

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	label('paymentReceived')
	input {
		triggeredBy('triggerPaymentReceived()')
	}
	outputMessage {
		sentTo('paymentsQueue')
		headers {
			messagingContentType(applicationJson())
		}
		body(
				'paymentId':'abc',
				'status' : 'PAID'
		)
	}
}