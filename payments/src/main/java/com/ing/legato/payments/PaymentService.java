package com.ing.legato.payments;

import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.MediaType;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
class PaymentService {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	private final PaymentsQueue paymentsQueue;

	PaymentService(PaymentsQueue paymentsQueue) {
		this.paymentsQueue = paymentsQueue;
	}

	String createPayment() {
		return UUID.randomUUID().toString();
	}

	String getPaymentStatus(String id) {
		LOG.info("Getting payment status from DB: {}", id);
		return "OK";
	}

	void paymentReceived(String id) {
		HashMap<String, Object> headers = new HashMap<>();
		headers.put("contentType", MediaType.APPLICATION_JSON_VALUE);

		paymentsQueue.paymentsQueue().send(
				MessageBuilder.createMessage(new Payment(id, PaymentStatus.PAID),
						new MessageHeaders(headers)));
	}
}
