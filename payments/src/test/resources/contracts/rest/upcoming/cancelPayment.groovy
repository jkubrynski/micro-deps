package contracts.rest

import org.springframework.cloud.contract.spec.Contract

Contract.make {
	request {
		method(DELETE())
		url("/payments/abc")
	}

	response {
		status(NO_CONTENT())
	}
}