package com.ing.legato.auctions;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = ":payments")
public class PaymentsClientTest {

	@Autowired
	PaymentsClient paymentsClient;

	@Test
	public void shouldCheckPaymentStatus() {
		String status = paymentsClient.getStatus("abc");
		assertThat(status).isEqualTo("OK");
	}

}